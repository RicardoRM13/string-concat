package com.ricardorm13.stringConcat.consumer.kafka;

import com.ricardorm13.stringConcat.consumer.service.messageService;
import lombok.extern.log4j.Log4j2;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class KafkaConsumer {
    @Autowired
    messageService _messageService;

    @KafkaListener(topics = "#{'${io.confluent.developer.config.topic.name}'}")
    public void listenWithHeaders(
            @Payload String message,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) throws Exception {
        System.out.println( "Received Message: " + message );

        JSONObject jsonObj = new JSONObject(message);

        _messageService.senMessage(jsonObj.getString("word1") + " " + jsonObj.getString("word2"));

        System.out.println("Concat: " + jsonObj.getString("word1") + " " + jsonObj.getString("word2"));
    }


}

