package com.ricardorm13.stringConcat.consumer.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.ricardorm13.stringConcat.consumer.entity.message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scripting.support.StandardScriptEvalException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.print.DocFlavor;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Service
public class messageService {
    @Autowired
    private message _message;

    @Autowired
    private Environment env;

    public message concatStrings(message m){
        this._message.setWord1(m.getWord1());
        this._message.setWord2(m.getWord2());
        return this._message;
    }

    public void senMessage(String messageConcat) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        String url = env.getProperty("APIUrl");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(jsonBody(messageConcat),headers);
        String answer = restTemplate.postForObject(url, entity, String.class);
        System.out.println(answer);
    }

    private String jsonBody(String messageConcat){
        return "{" +
                "\"notification\":" +
                "{            \"title\": \"Words Events\"," +
                                "\"body\": \"" + messageConcat + "\"," +
                                "\"icon\": \"assets/main-page-logo-small-hat.png\", " +
                                "\"vibrate\": [100, 50, 100]," +
                "\"data\": {" +
                    "\"dateOfArrival\": \"" + LocalDateTime.now() + "\"," +
                    "\"primaryKey\": 1            }," +
                 "\"actions\": [{" +
                    "\"action\": \"http://localhost:8081\"," +
                    "\"title\": \"Go to the site\" }]" +
                "} }";

    }
}
