import { Component, OnInit } from '@angular/core';
import { WORDS } from '../mock-words';
import { Words } from '../words';
import { WordsService} from '../words.service';


@Component({
  selector: 'app-words',
  templateUrl: './words.component.html',
  styleUrls: ['./words.component.css']
})
export class WordsComponent implements OnInit {

  success = "";

  word : Words={
    word1: "",
    word2: "",
    concat: ""
  };

  constructor(private wordsService: WordsService) { }

  getWords(): void {
    this.wordsService.getWords()
      .subscribe(words => this.word = words)
  }

  send(): void {
    this.success ="sending ..."
    
    this.wordsService.postWords(this.word)
      .subscribe(x=>{
        this.success = "Words sent successfuly"
      },err=>{
        this.success = "Somenthing got wrong"
      })
      
  }

  ngOnInit(): void {
    this.getWords();
  }

}
