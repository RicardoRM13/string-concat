import { Injectable } from '@angular/core';
import { Words} from '../app/words'
import { WORDS } from './mock-words';
import { Observable, of } from 'rxjs';
import {map} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {environment} from '../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class WordsService {

  constructor(private http: HttpClient) { }

  getWords(): Observable<Words>{
    WORDS.concat = WORDS.word1 + " " + WORDS.word2;
    const words = of(WORDS);
    return words;
  }


  postWords(ws: Words)
  {
    return this.http.post(environment.baseURL + 'words', ws).pipe(map(res =>res));
  }

}
