import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {map} from 'rxjs/operators';
import {environment} from '../environments/environment.prod';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NotificationServiceService {

  constructor(private http: HttpClient) { }

  subscribe(subscription:any){
    return this.http.post(environment.baseURL+'subscribe',subscription).pipe(map(res=>res));
  }
  
  triggerMessage(message: string){
    return this.http.post(environment.baseURL+'message',JSON.parse(message)).pipe(map(res=>res));
  }

  test()
  {
    return this.http.get(environment.baseURL+'test').pipe(map(res=>res));
  }
}
