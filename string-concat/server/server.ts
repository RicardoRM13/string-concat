
import * as express from 'express';
import {Application} from "express";
import {addPushSubscriber} from "./add-push-subscriber.route";
import {sendNewsletter} from "./send-newsletter.route";
import {sendWords} from "./send-words";
import {AddressInfo} from 'net';
const bodyParser = require('body-parser');

const webpush = require('web-push');

const vapidKeys = {
    "publicKey":"BMrMXUgjo9Q-rVT5N-Xe7LXFoFtj79trUhxaYnKQnxbGdlYRugGaS-lxaEOI0W_vxtOEs_Qz79lmcnaKHno_v7U",
    "privateKey":"eZTjPPCRbt2xLcZclNnVN4lJAfjJxYS9-IGmll5MTJY"
};


webpush.setVapidDetails(
    'mailto:rycardo.marques@terra.com.br',
    vapidKeys.publicKey,
    vapidKeys.privateKey
);




const app: Application = express();


app.use(bodyParser.json());


// REST API

app.route('/api/subscribe')
    .post(addPushSubscriber);

app.route('/api/message')
    .post(sendNewsletter);

app.route('/api/words')
    .post(sendWords);



// launch an HTTP Server
const httpServer = app.listen(8084, () => {
    const { port } = httpServer.address() as AddressInfo;
    console.log("HTTP Server running at http://localhost:" + port);
});









