import * as _ from 'lodash';
import {USER_SUBSCRIPTIONS} from "./in-memory-db";


class InMemoryDatabase {

    readAllLessons() {
        return _.values(USER_SUBSCRIPTIONS);
    }

}




export const db = new InMemoryDatabase();