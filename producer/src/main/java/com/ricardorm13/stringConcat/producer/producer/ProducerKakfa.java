package com.ricardorm13.stringConcat.producer.producer;

import com.ricardorm13.stringConcat.producer.entity.message;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@RequiredArgsConstructor
public class ProducerKakfa {
    @Autowired
    private Environment env;

    @Autowired
    private KafkaTemplate<String, message> kafkaTemplate;


    public void sendMessage(message _message) {

        ListenableFuture<SendResult<String, message>> future =
                kafkaTemplate.send(env.getProperty("topicName"), _message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, message>>() {

            @Override
            public void onSuccess(SendResult<String, message> result) {
                System.out.println("Sent message with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message due to : " + ex.getMessage());
            }
        });
    }

}
