package com.ricardorm13.stringConcat.producer.service;

import com.ricardorm13.stringConcat.producer.entity.message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.interceptor.StatefulRetryOperationsInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

@Service
public class messageService {
    @Autowired
    private message _message;

    public message concatStrings(message m){
        this._message.setWord1(m.getWord1());
        this._message.setWord2(m.getWord2());
        return this._message;
    }
}
