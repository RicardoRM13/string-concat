package com.ricardorm13.stringConcat.producer.entity;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
public class message {
    private String word1;
    private String word2;

    public message(){

    }

    public message(String w1, String w2, String concat){
        this.word1 = w1;
        this.word2 = w2;
    }

    public String getWord1() {
        return word1;
    }

    public void setWord1(String word1) {
        this.word1 = word1;
    }

    public String getWord2() {
        return word2;
    }

    public void setWord2(String word2) {
        this.word2 = word2;
    }

}
