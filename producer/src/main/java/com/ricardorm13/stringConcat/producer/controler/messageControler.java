package com.ricardorm13.stringConcat.producer.controler;

import com.ricardorm13.stringConcat.producer.entity.message;
import com.ricardorm13.stringConcat.producer.producer.ProducerKakfa;
import com.ricardorm13.stringConcat.producer.service.messageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class messageControler {
    @Autowired
    private messageService _mService;

    @Autowired
    private  message _message;

    @Autowired
    private ProducerKakfa _producer;

    @RequestMapping(value = "/words", method =  RequestMethod.POST)
    public HttpStatus postConcat(@RequestBody message m){
        try {
            System.out.println("enviando mensagem");
            _producer.sendMessage(m);
        }catch (Exception e){
            System.out.println(e);
            return HttpStatus.UNPROCESSABLE_ENTITY;
        }

        return HttpStatus.CREATED;
    }

}
