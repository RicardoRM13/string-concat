# strint-concat async

This project has all necessary artitacts for implementation of a front end with asynchronous push notifications.

This solution was built over Angular, Kafka Events and Push Notifications:

![Screenshot](Solution.png)

**Directories**

- string-contat: The Angular project frontend
- consumer: The kafka consumer project
- producer: The API and kafka producer
- kakfa: The docker-compose for kafka node
